import { Foo } from '../../src/contracts/foo'
import { getDefaultSigner, inputSatoshis } from '../utils/txHelper'
import { toByteString, sha256 } from 'scrypt-ts'

const message = 'hello world, sCrypt!'

async function main() {
    await Foo.compile()
    const instance = new Foo(sha256(toByteString(message, true)))

    // connect to a signer
    await instance.connect(getDefaultSigner())

    // contract deployment
    const deployTx = await instance.deploy(inputSatoshis)
    console.log('Foo contract deployed: ', deployTx.id)

    // contract call
    const { tx: callTx } = await instance.methods.unlock(
        toByteString(message, true)
    )
    console.log('Foo contract `unlock` called: ', callTx.id)
}

describe('Test SmartContract `Foo` on testnet', () => {
    it('should succeed', async () => {
        await main()
    })
})
