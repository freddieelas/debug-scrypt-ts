import {
    assert,
    ByteString,
    method,
    prop,
    PubKey,
    hash160,
    Sig,
    SigHashPreimage,
    SmartContract,
    toByteString,
    PubKeyHash,
    SigHash,
    exit,
    byteString2Int,
} from 'scrypt-ts'

export class Trade extends SmartContract {
    @prop()
    pubKeyHash: PubKeyHash
    @prop()
    adminPubKeyHash: PubKeyHash
    static jsonFormatHash = toByteString(
        '097f5e99c11448837379c0c9ba1fd2e531fd5a49370c06d60aa775ce36478d07'
    )
    static expectedMsgType = toByteString('TRADE', true)
    constructor(pubKeyHash: PubKeyHash, adminPubKeyHash: PubKeyHash) {
        super(...arguments)
        this.pubKeyHash = pubKeyHash
        this.adminPubKeyHash = adminPubKeyHash
    }
    @method()
    public unlock(
        sig: Sig,
        pubKey: PubKey,
        context: SigHashPreimage,
        tradeJSON: ByteString,
        breakdownOffsets: ByteString
    ) {
        // - Drops tokenovate off stack (SKIPPING)
        // - Checks if admin pukey if so checksig that and return
        if (hash160(pubKey) == this.adminPubKeyHash) {
            if (this.checkSig(sig, pubKey)) exit(true)
        }
        // - Otherwise checksig against pubKeyHash
        // Check public keys belong to the specified addresses
        assert(hash160(pubKey) == this.pubKeyHash, 'Wrong spender pub key')
        assert(this.checkSig(sig, pubKey), 'Spender sig invalid')

        // - extract data from specifically ordered json
        let idx = 0n
        const offset = byteString2Int(
            breakdownOffsets.slice(Number(idx), Number((idx += 1n)))
        )
        const len = byteString2Int(
            breakdownOffsets.slice(Number(idx), Number((idx += 1n)))
        )

        const sessionId = tradeJSON.slice(Number(offset), Number(offset + len))
        idx += len
        let buyerPublicKey
        let buyerCustodian
        let sellerPublicKey
        let sellerCustodian
        let executionId
        let symbol
        let quantity
        let price
        let transactionTime
        let msgOrigin
        let msgUID
        let msgType
        let msgTimestamp
        let captureTimestamp
        let eventID
        /*
        
*/
        // - build output that we expect at this stage
        // - splits input script outpoint
        // - checks value is 1
        // - check nSequence is not final (nLockTime governs when onchain)
        // - pop blob off script code ()
        // - if www.tokenovate.com
        //     - if msgType 'NOVATION' (calc some kind of hash based off trade ledger hash)
        //         various stage json format checks
        // - else
        //     newOrigin == oldOrigin
        // - else if trade complete
        //     check stuff
        //     check nLockTime = timestamp
        // - if msgtype not in bounds return error
        // - finish output and hashOutputs
        // - Checks preimage
        assert(this.checkPreimageSigHashType(context, SigHash.ALL))
    }
}
